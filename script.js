document.addEventListener('DOMContentLoaded', function () {
    const languageSelect = document.getElementById('language-select');
    const form = document.getElementById('phq8-form');
    const resultDiv = document.getElementById('result');
    let scores = new Array(8).fill(0);

    languageSelect.addEventListener('change', function () {
        generateQuestions(this.value);
    });

    generateQuestions(languageSelect.value);

    form.addEventListener('submit', function (event) {
        event.preventDefault();
        calculateResult(languageSelect.value);
    });
});

function generateQuestions(lang) {
    const translations = {
        "en": {
            "questions": [
                "Little interest or pleasure in doing things?",
                "Feeling down, depressed, or hopeless?",
                "Trouble falling or staying asleep, or sleeping too much?",
                "Feeling tired or having little energy?",
                "Poor appetite or overeating?",
                "Feeling bad about yourself — or that you are a failure or have let yourself or your family down?",
                "Trouble concentrating on things, such as reading the newspaper or watching television?",
                "Moving or speaking so slowly that other people could have noticed? Or the opposite — being so fidgety or restless that you have been moving around a lot more than usual?"
            ]
        },
        "pl": {
            "questions": [
                "Małe zainteresowanie lub przyjemność z robienia rzeczy?",
                "Poczucie przygnębienia, depresji lub beznadziei?",
                "Problemy z zasypianiem lub przesypianiem nocy, lub nadmierna senność?",
                "Uczucie zmęczenia lub brak energii?",
                "Zły apetyt lub przejadanie się?",
                "Złe samopoczucie – poczucie, że jesteś nieudacznikiem, albo że zawiódłeś siebie lub rodzinę?",
                "Problemy z koncentracją na rzeczach, takich jak czytanie gazet czy oglądanie telewizji?",
                "Poruszanie się lub mówienie tak wolno, że inni mogli to zauważyć? Albo przeciwnie – być tak niespokojnym, że dużo się więcej poruszasz niż zwykle?"
            ]
        }
    };

    const form = document.getElementById('phq8-form');
    form.innerHTML = ''; // Clear existing content
    const questions = translations[lang]["questions"];
    let scores = new Array(questions.length).fill(0);

    questions.forEach((question, i) => {
        const div = document.createElement('div');
        div.className = 'question';

        const label = document.createElement('label');
        label.textContent = question;
        div.appendChild(label);

        const emojis = ['😄', '😐', '😕', '😭'];
        const emojisDiv = document.createElement('div');
        emojisDiv.className = 'emojis';

        emojis.forEach((emoji, j) => {
            const span = document.createElement('span');
            span.className = 'emoji';
            span.textContent = emoji;
            span.dataset.score = j;
            span.dataset.question = i;
            span.addEventListener('click', function (event) {
                document.querySelectorAll(`[data-question="${i}"].selected`).forEach(e => e.classList.remove('selected'));
                event.currentTarget.classList.add('selected');
                scores[i] = j;
            });
            emojisDiv.appendChild(span);
        });

        div.appendChild(emojisDiv);
        form.appendChild(div);
    });

    // Add the submit button
   const submitButton = document.createElement('button');
   submitButton.type = 'submit';
   submitButton.textContent = lang === 'en' ? 'Submit' : 'Zatwierdź';
   submitButton.className = 'submit-button'; // Add this line
   form.appendChild(submitButton);
}

function calculateResult(lang) {
    const resultMessages = {
        "en": {
            "severe": "There is a high risk of depression. We recommend contact with a doctor. If you need to talk to someone immediately, call 800 12 12 12 or write to czat.brpd.gov.pl.",
            "fine": "It seems that everything is fine. But if you're feeling bad, it's worth consulting a doctor."
        },
        "pl": {
            "severe": "Istnieje duże ryzyko depresji. Zalecamy kontakt z lekarzem. Jak potrzebujesz z kimś porozmawiać natychmiast, zadzwoń na  800 12 12 12 lub napisz na czat.brpd.gov.pl.",
            "fine": "Wydaje się, że wszystko jest w porządku. Ale jeśli źle się czujesz, warto skonsultować się z lekarzem."
        }
    };

    const scores = Array.from(document.querySelectorAll('.emoji.selected')).map(e => parseInt(e.dataset.score));
    const totalScore = scores.reduce((a, b) => a + b, 0);
    let message;

    if (totalScore >= 10) {
        message = resultMessages[lang]["severe"];
    } else {
        message = resultMessages[lang]["fine"];
    }

    document.getElementById('result').textContent = message;
}
document.addEventListener('DOMContentLoaded', function () {
    const languageSelect = document.getElementById('language-select');
    const form = document.getElementById('phq8-form');
    const resultDiv = document.getElementById('result');
    let scores = new Array(8).fill(0);

    languageSelect.addEventListener('change', function () {
        generateQuestions(this.value);
        updateLegend(this.value);
    });


    generateQuestions(languageSelect.value);
    updateLegend(languageSelect.value);

    form.addEventListener('submit', function (event) {
        event.preventDefault();
        calculateResult(languageSelect.value);
    });
});

function updateLegend(lang) {
    const legendTranslations = {
        "en": ["Not at all", "Sometimes", "Often", "Every day"],
        "pl": ["Wcale nie", "Czasami", "Często", "Codziennie"]
    };

    document.querySelectorAll('.legend p').forEach((p, index) => {
        p.childNodes[1].textContent = legendTranslations[lang][index];
    });
}

document.addEventListener('DOMContentLoaded', function () {
    const languageSelect = document.getElementById('language-select');
    const form = document.getElementById('phq8-form');
    const title = document.getElementById('questionnaire-title'); // Reference to the title
    let scores = new Array(8).fill(0);

    // Function to update content based on selected language
    function updateContent(lang) {
        // Update the title
        title.textContent = lang === 'en' ? 'PHQ-8 Questionnaire' : 'Kwestionariusz PHQ-8';

        // Update the questions
        generateQuestions(lang);

        // Update the legend
        updateLegend(lang);
    }

    languageSelect.addEventListener('change', function () {
        updateContent(this.value);
    });

    // Initial update on page load
    updateContent(languageSelect.value);

    form.addEventListener('submit', function (event) {
        event.preventDefault();
        calculateResult(languageSelect.value);
    });
});
